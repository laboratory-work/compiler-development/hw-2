package ru.lanolin.hw2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DFA {

	private final int startState;

	private final List<Integer> endStates;
	private final List<Map<Character, Integer>> transmitions;

	public DFA(int countState, List<Transition> transitions, int startState, List<Integer> endStates) {
		this.startState = startState;
		this.endStates = endStates;
		this.transmitions = new ArrayList<>(countState);
		for (int i = 0; i < countState; i++) {
			transmitions.add(i, new HashMap<>());
		}

		for (Transition trans : transitions) {
			int fromState = trans.getFromState();
			char recognize = trans.getRecognize();
			int toState = trans.getToState();
			transmitions.get(fromState).putIfAbsent(recognize, toState);
		}
	}

	public boolean recognize(String str) {
		char[] strChars = str.toCharArray();
		int strLength = str.length();

		int currentState = startState;
		for (int i = 0; i < strLength; i++) {
			char ch = strChars[i];
			Map<Character, Integer> availableTransmission = transmitions.get(currentState);
			if (availableTransmission.containsKey(ch)) {
				currentState = availableTransmission.get(ch);
			} else {
				return false;
			}
		}

		return endStates.contains(currentState);
	}

}
