package ut.ru.lanolin.hw2;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.lanolin.hw2.DFA;
import ru.lanolin.hw2.Transition;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DFATest {

	private static List<Transition> transitions;
	private static List<Integer> endStates;
	private static int countStates;
	private static int startState;

	@BeforeAll
	static void beforeAll() {
		countStates = 3;
		startState = 1;
		transitions = Arrays.asList(
				new Transition(1, 'a', 1),
				new Transition(1, 'b', 1),
				new Transition(1, 'c', 2),
				new Transition(2, 'c', 3)
		);
		endStates = Collections.singletonList(3);
	}

	private DFA dfa;

	@BeforeEach
	void setUp() {
		this.dfa = new DFA(countStates, transitions, startState, endStates);
	}

	static Stream<Arguments> createStringRecognize() {
		return Stream.of(
				Arguments.arguments("aaaaaaabababcc", true),
				Arguments.arguments("acabababababcc", false),
				Arguments.arguments("acc", true),
				Arguments.arguments("cc", true),
				Arguments.arguments("c", false),
				Arguments.arguments("a", false),
				Arguments.arguments("aab", false),
				Arguments.arguments("bcc", true),
				Arguments.arguments("ac", false)
		);
	}

	@DisplayName("Проверка работы конечного автомата")
	@ParameterizedTest(name = "{index}: recognaze({0}) = {1}")
	@MethodSource("createStringRecognize")
	void testDFARecognize(String str, boolean expectedValue) {
		boolean recognize = dfa.recognize(str);
		assertEquals(expectedValue, recognize);
	}

	@AfterEach
	void tearDown() {
		this.dfa = null;
	}

	@AfterAll
	static void afterAll() {
		transitions = null;
		endStates = null;
	}
}