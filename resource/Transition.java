package ru.lanolin.hw2;

import lombok.*;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Transition {
	private final int fromState;
	private final char recognize;
	private final int toState;
}
